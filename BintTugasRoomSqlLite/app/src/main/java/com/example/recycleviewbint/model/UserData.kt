package com.example.bintrecycleview.model

data class UserData(
    val userName: String,
    val nim: String
)
