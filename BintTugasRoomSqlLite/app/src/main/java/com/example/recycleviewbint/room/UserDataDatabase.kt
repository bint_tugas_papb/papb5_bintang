package com.example.recycleviewbint.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [UserDataEntity::class], version = 1)

abstract class UserDataDatabase : RoomDatabase() {

    abstract fun userDataDao(): UserDataDao

    companion object {
        private var instance: UserDataDatabase? = null

        fun getInstance(context: Context): UserDataDatabase {
            if (instance == null) {
                synchronized(UserDataDatabase::class) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        UserDataDatabase::class.java,
                        "user_data_database"
                    ).build()
                }
            }
            return instance!!
        }
    }
}
