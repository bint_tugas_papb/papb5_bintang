package com.example.recycleviewbint

import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.EditText
import android.widget.RelativeLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bintrecycleview.model.UserData
import com.example.recycleviewbint.room.UserDataDatabase
import com.example.recycleviewbint.room.UserDataEntity
import com.google.android.material.floatingactionbutton.FloatingActionButton

class MainActivity : AppCompatActivity() {
    private lateinit var button: FloatingActionButton
    private lateinit var recv: RecyclerView
    private lateinit var userList: ArrayList<UserData>
    private lateinit var userAdapter: UserAdapter
    private lateinit var emptyRelativeLayout: RelativeLayout
    private lateinit var database: UserDataDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.ic_add)
        recv = findViewById(R.id.Recycler)
        emptyRelativeLayout = findViewById(R.id.emptyRelativeLayout)

        // Initialize the database
        database = UserDataDatabase.getInstance(applicationContext)

        // Initialize the RecyclerView and its adapter
        userList = ArrayList()
        userAdapter = UserAdapter(this, userList, emptyRelativeLayout)
        recv.adapter = userAdapter
        recv.layoutManager = LinearLayoutManager(this)

        loadDataFromDatabase()

        button.setOnClickListener {
            showCustomAlertDialog()
        }
    }

    private fun showCustomAlertDialog() {
        val inflater = layoutInflater
        val dialogView = inflater.inflate(androidx.core.R.layout.custom_dialog, null)

        val nameEditText = dialogView.findViewById<EditText>(R.id.name)
        val nimEditText = dialogView.findViewById<EditText>(R.id.nimEditText)

        AlertDialog.Builder(this)
            .setTitle("Add User")
            .setView(dialogView)
            .setPositiveButton("Add") { _, _ ->
                val name = nameEditText.text.toString()
                val nim = nimEditText.text.toString()
                if (name.isNotEmpty() && nim.isNotEmpty()) {
                    saveUserDataToDatabase(name, nim)
                }
            }
            .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun saveUserDataToDatabase(name: String, nimText: String) {
        val userDataEntity = UserDataEntity(userName = name, nim = nimText)
        database.userDataDao().insert(userDataEntity)
        loadDataFromDatabase()
    }

    private suspend fun loadDataFromDatabase() {
        val userDataList = database.userDataDao().getAllUserData()
        userList.clear()
        userList.addAll(userDataList.map { UserData(it.userName, it.nim) })
        userAdapter.setData(userList)
    }
}
