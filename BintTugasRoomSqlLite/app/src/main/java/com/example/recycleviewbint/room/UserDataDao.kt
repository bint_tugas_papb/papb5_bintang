package com.example.recycleviewbint.room
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserDataDao {
    @Insert
    suspend fun insert(userData: UserDataEntity)

    @Query("SELECT * FROM user_data")
    suspend fun getAllUserData(): List<UserDataEntity>
}
