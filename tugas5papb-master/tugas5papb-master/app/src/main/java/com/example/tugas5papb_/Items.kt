package com.example.tugas5papb_

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Items  (
    val profile: Int,
    val nama: String,
    val nim: String
) : Parcelable