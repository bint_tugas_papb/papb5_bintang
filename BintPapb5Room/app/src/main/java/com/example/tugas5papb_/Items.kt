package com.example.tugas5papb_

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Items  (
    val img: Int,
    val Nama: String,
    val nim: String
) : Parcelable